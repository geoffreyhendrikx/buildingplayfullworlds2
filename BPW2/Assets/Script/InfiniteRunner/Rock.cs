﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rock : MonoBehaviour
{

    private void OnCollisionEnter(Collision coll)
    {
        BoatController boat = coll.gameObject.GetComponent<BoatController>();
        if(boat)
            coll.gameObject.GetComponent<BoatController>().Overlay.GotHit();
        gameObject.SetActive(false);
    }
    
    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
