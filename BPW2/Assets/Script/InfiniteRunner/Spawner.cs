﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] rocks;
    [SerializeField]
    private GameObject pirateShip;
    private bool isSailing;
    private List<Rigidbody> rockRigidbodys = new List<Rigidbody>();
    private List<Rigidbody> pirateShipRigidbodys = new List<Rigidbody>();
    [SerializeField]
    private Transform[] spawnPositions;
    [SerializeField]
    private Transform[] aiSpawns;

    [SerializeField]
    private float difficulty;
    [SerializeField ]
    private BoatController boatController;
    private bool isGeneratingPirateship;
    private void Start()
    {
        StartCoroutine(GenerateRock());
        StartCoroutine(GeneratePirateShip());
    }

    private void Update()
    {
        for (int i = 0; i < rockRigidbodys.Count; i++)
            if(rockRigidbodys[i] != null)
                rockRigidbodys[i].velocity = new Vector3(0, 0, -150);

    }

    private IEnumerator GenerateRock()
    {
        GameObject rock = Instantiate(rocks[Random.Range(0, rocks.Length)]);
        Rigidbody rb;
        rb = rock.GetComponent<Rigidbody>();
        rockRigidbodys.Add(rb);
        rock.transform.position = spawnPositions[Random.Range(0, spawnPositions.Length)].position;

        yield return new WaitForSeconds(difficulty);
        StartCoroutine(GenerateRock());
    }

    /// <summary>
    /// Generate pirate ship
    /// </summary>
    /// <returns></returns>
    private IEnumerator GeneratePirateShip()
    {
        Rigidbody rb;
        PirateShip pirateScript = pirateShip.GetComponent<PirateShip>();

        rb = pirateShip.GetComponent<Rigidbody>();
        pirateScript.GenerateKey();

        Vector3 position = aiSpawns[Random.Range(0, aiSpawns.Length)].position;

        pirateShip.transform.position = new Vector3(position.x, -180, -1220);

        UnityAction unityAction = null;
        unityAction += pirateScript.stopWatch.BeginTimer;

        StartCoroutine(Extensions.LerpingObjects(pirateShip.transform, aiSpawns[Random.Range(0, aiSpawns.Length)].position, 5));
        StartCoroutine(Extensions.Wait(5,unityAction));

        yield return null;
    }

    /// <summary>
    /// Spawning new Pirate ship.
    /// </summary>
    public void SpawnNewPirateShip()
    {
            StartCoroutine(GeneratePirateShip());
    }

}