﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class MenuState : State<GameState>
{
    private UnityAction unityAction;
    private bool parented;
    private float distance;
    private float timeStamp = 5;
    private bool checkCompass = true;

    [SerializeField]
    private float amplitude;
    [SerializeField]
    private float frequency;

    private InputManager inputManager
    {
        get
        {
            return GameManager.Instance.GetManager<InputManager>();
        }
    }

    public MenuState(GameState owner) : base(owner)
    {
    }

    // Start is called before the first frame update
    public override void Enter()
    {
        base.Enter();
        //Toggle camera to go to the sailing scene.
        Owner.NarativeManager.ToggleCamera();
        //filter removal
        GameManager.Instance.StartCoroutine(Extensions.FadeImage(Owner.Filter, new Color(0, 0, 0, 0), 5));
        GameManager.Instance.StartCoroutine(WaitFewSeconds());


    }

    public override void Execute()
    {
        base.Execute();
        if(inputManager.Boat)
            inputManager.Boat.transform.rotation = Quaternion.Euler(amplitude * Mathf.Sin(Time.time * frequency),
                    inputManager.Boat.transform.rotation.eulerAngles.y,
                    amplitude * Mathf.Cos(Time.time * frequency));
    }

    public IEnumerator WaitFewSeconds()
    {
        yield return new WaitForSeconds(timeStamp);
        base.Owner.PirateMenu.SetActive(true);
        if (Owner.ObjectiveManager == null)
            Owner.ObjectiveManager = GameManager.Instance.GetManager<ObjectiveManager>();



    }

    public override void Exit()
    {
        base.Exit();
    }
}
