﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicState : State<GameState>
{
    bool dontCheck = false;

    public CinematicState(GameState owner) : base(owner)
    {
    }


    /// <summary>
    /// Enter is called in the start methode
    /// </summary>
    public override void Enter()
    {
        base.Enter();
        Debug.Log("Enter");
        Owner.InputManager.InputEnabled = false;
        Owner.NarativeManager.ToggleNarative();
    }

    /// <summary>
    /// Updating the state
    /// </summary>    
    public override void Execute()
    {
        base.Execute();
        if (Owner.InputManager.NarativeStory() && !dontCheck)
        {
            if (Owner.NarativeManager.NextLine())
                GameManager.Instance.StartCoroutine(WaitDialog());
            else
            {
                Owner.StateMachine.ChangeState(Owner.AvailableStates[(int)States.MenuState]);
            }
        }
    }

    public override void Exit()
    {
        base.Exit();
        Owner.NarativeManager.ToggleNarative();
    }

    private IEnumerator WaitDialog()
    {
        dontCheck = true;
        yield return new WaitForSeconds(.1f);
        dontCheck = false;
    }

}
