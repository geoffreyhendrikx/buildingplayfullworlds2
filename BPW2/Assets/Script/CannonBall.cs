﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CannonBall : MonoBehaviour
{
    [SerializeField]
    private Rigidbody rb;
    [SerializeField]
    private ParticleSystem particle;
    [SerializeField]
    private Spawner spawner;
    private PirateShip pirateShip;
    private void OnBecameInvisible() => Destroy(this.gameObject);

    public void Start()
    {
        spawner = FindObjectOfType<Spawner>();
        pirateShip = FindObjectOfType<PirateShip>();
        particle.Stop();
    }

    public void CannonEffect()
    {
        particle.Play();
        rb.velocity = Vector3.zero;
        UnityAction unityAction = DestroyGameObject;
        StartCoroutine(Extensions.Wait(particle.main.duration, unityAction));
    }

    private void DestroyGameObject()
    {
        Destroy(this.gameObject);
    }
}
