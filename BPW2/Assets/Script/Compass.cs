﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compass : MonoBehaviour
{
    [SerializeField]
    private Text CompassText;

    private CompassDirection compassDirection;
    public CompassDirection CompassDirection
    {
        get
        {
            return compassDirection;
        }
        set
        {
            compassDirection = value;
        }
    }

    [SerializeField]
    private Transform boat;

    private float offset = 90;

    private bool foundTreasure;
    public bool FoundTreasure
    {
        get
        {
            return foundTreasure;
        }
        set
        {
            foundTreasure = value;
        }
    }


    // Update is called once per frame
    private void Update()
    {
        #if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.X))
            foundTreasure = !foundTreasure;
        #endif

        Vector3 boatForward = boat.transform.forward.normalized;

        if (!foundTreasure)
        {
            transform.LookAt(Vector3.forward);
            transform.rotation *= Quaternion.Euler(0, 90, 0);
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        }
        else
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y - 10 % 360, 0);


        //North
        if (Vector3.Angle(this.boat.transform.forward, Vector3.forward) <= 45)
        {
            compassDirection = CompassDirection.North;
            if (CompassText)
                CompassText.text = "North";
        }
        //East
        else if (Vector3.Angle(this.boat.transform.forward, Vector3.right) <= 45)
        {
            compassDirection = CompassDirection.East;
            if(CompassText)
                CompassText.text = "East";
        }
        //South
        else if (Vector3.Angle(this.boat.transform.forward, Vector3.back) <= 45)
        {
            compassDirection = CompassDirection.South;
            if(CompassText)
                CompassText.text = "South";
        }

        //West
        else
        {
            compassDirection = CompassDirection.West;
            if(CompassText)
                CompassText.text = "West";
        }

    }
}
public  enum CompassDirection
{
    North,
    East,
    South,
    West
};