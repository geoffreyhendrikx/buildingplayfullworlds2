﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PirateShip : MonoBehaviour
{
    [SerializeField]
    private List<KeyBinds> keyBinds;
    [SerializeField]
    private Image image;
    [SerializeField]
    private BoatController boatController;

    //this is the keybind the player has to press
    private KeyBinds keyBind;
    public KeyBinds KeyBind
    {
        get
        {
            return keyBind;
        }
        set
        {
            keyBind = value; 
        }
    }

    public bool canCheck;
    [SerializeField]
    public PirateTimer stopWatch;
    private Vector3 beginPosition;
    [SerializeField]
    private Spawner spawner;
    private bool pirateshipHasShoot;
    [SerializeField]
    private Transform[] cannons;
    [SerializeField]
    private ParticleSystem[] particleSystems;
    [SerializeField]
    private float cannonballSpeed;

    public bool CanShoot;

    // Start is called before the first frame update
    private void Start()
    {
        beginPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (stopWatch.TimerToggle && CanShoot)
            image.fillAmount = stopWatch.PirateStopWatch;
        if (!pirateshipHasShoot && image.fillAmount == 0)
            Attack();


    }

    public void GenerateKey()
    {
        KeyBind = keyBinds[Random.Range(0, keyBinds.Count)];
        image.sprite = KeyBind.KeySprite;
        stopWatch.ResetTimer();
        canCheck = true;
        CanShoot = true;
        pirateshipHasShoot = false;
        image.fillAmount = 1;

    }

    public void OnCollisionEnter(Collision coll)
    {
        CannonBall cannonball = coll.gameObject.GetComponent<CannonBall>();

        if (!cannonball)
            return;

        //Activate particle effect
        cannonball.CannonEffect();

        LerpingShipBack();
    }

    public void LerpingShipBack()
    {
        //lerping back to the old position.
        StartCoroutine(Extensions.LerpingObjects(this.gameObject.transform, beginPosition, 3f));
        canCheck = false;

        //Waiting till spawning a new pirate ship
        UnityAction unityAction = spawner.SpawnNewPirateShip;
        StartCoroutine(Extensions.Wait(6f, unityAction));

        stopWatch.ResetTimer();
    }

    public void Attack()
    {
        for (int i = 0; i < cannons.Length; i++)
        {
            GameObject cannonball = GameObject.Instantiate(Resources.Load<GameObject>("Cannonball"), cannons[i].transform.position, cannons[i].transform.rotation, null);
            cannonball.GetComponent<Rigidbody>().AddForce(cannons[i].transform.right * (cannonballSpeed));
            particleSystems[i].Play();

        }
        pirateshipHasShoot = true;
        UnityAction unityAction = LerpingShipBack;
        StartCoroutine(Extensions.Wait(2, unityAction));
    }

}

[System.Serializable]
public struct KeyBinds
{
    [SerializeField]
    private KeyCode keyCode;
    public KeyCode KeyCode
    {
        get
        {
            return keyCode;
        }
        set
        {
            keyCode = value;
        }
    }

    [SerializeField]
    private Sprite keySprite;
    public Sprite KeySprite
    {
        get
        {
            return keySprite;
        }
        set
        {
            keySprite = value;
        }
    }
         
}