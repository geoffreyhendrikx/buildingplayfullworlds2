﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BoatController : Actor
{
    [Header("Movement options")]
    [SerializeField]
    [Range(0, 5)]
    private float speed;
    [SerializeField]
    [Range(0, 15)]
    private float maxSpeed;

    [Header("Rotation")]
    [SerializeField]
    private GameObject steerDirection;
    [SerializeField]
    private float rotationSpeed;

    [SerializeField]
    private Transform[] cannons;
    [SerializeField]
    private ParticleSystem[] particleSystems;
    [SerializeField]
    private float cannonballSpeed;

    [SerializeField]
    private Transform[] lanes;

    [SerializeField]
    [Range(0,5)]
    private float amplitude;
    [SerializeField]
    [Range(0,5)]
    private float frequency;

    private UnityAction toggleMoving;

    private int currentLane = 1;

    private bool canMove = true;

    private KeyBinds keyBind;

    [SerializeField]
    private ScoreOverlay overLay;
    public ScoreOverlay Overlay
    {
        get
        {
            return overLay;
        }

        set
        {
            overLay = value;
        }
    }

    private static bool canBeHitted;

    private void Start()
    {
        canBeHitted = true;
        toggleMoving += ToggleMoving;
    }

    private void Update()
    {

        transform.rotation = Quaternion.Euler(amplitude * Mathf.Sin(Time.time * frequency),
                            transform.rotation.eulerAngles.y,
                            amplitude * Mathf.Cos(Time.time * frequency));

    }

    public void MovingLane(float axis)
    {
        if (canMove)
        {
            toggleMoving.Invoke();


            if (axis < 0)
            {
                if (currentLane > 0)
                    currentLane--;
                else
                    return;
            }
            else
            {
                if (currentLane < 2)
                    currentLane++;
                else
                    return;
            }

            StartCoroutine(Extensions.LerpingObjects(this.transform, lanes[currentLane].transform.position, 1, toggleMoving));
        }

    }

    public void ToggleMoving()
    {
        canMove = !canMove;
    }

    /// <summary>
    /// Moving the rigidbody with force in the x and z axis.
    /// </summary>
    /// <param name="rb">Rigidbody of the object</param>
    /// <param name="inputX">X Input</param>
    /// <param name="inputZ">Z Input</param>
    /// <param name="speed">Speed of the Object</param>
    public void MoveRigidbody(float inputZ)
    {
        Vector3 Movement;
        if (GameManager.Instance.GetSceneIndex() != 1)
            Movement = (MyRb.gameObject.transform.forward * (inputZ * (speed * 2)));
        else
        {
            MyRb.velocity = Vector3.zero;
            Movement = (transform.forward + MyRb.gameObject.transform.forward * (inputZ * (speed * 100)));
        }
        //check the speed of  the boat
        if (MyRb.velocity.magnitude < maxSpeed)
            MyRb.AddForce(Movement);

    }

    /// <summary>
    /// Rotate the boat.
    /// </summary>
    public void RotateBoat(float inputX)
    {
        Debug.Log("Rotate");
        MyRb.transform.eulerAngles = new Vector3(MyRb.transform.eulerAngles.x, MyRb.transform.eulerAngles.y + (inputX * rotationSpeed), MyRb.transform.eulerAngles.z);
    }

    public void Attack()
    {
        overLay.AddScore(100);
        for (int i = 0; i < cannons.Length; i++)
        {
            //Debug.Log("SpawnCannonBall");
            GameObject cannonball = GameObject.Instantiate(Resources.Load<GameObject>("Cannonball"),cannons[i].transform.position,cannons[i].transform.rotation,null);
            cannonball.GetComponent<Rigidbody>().AddForce( cannons[i].transform.right * (cannonballSpeed));
            particleSystems[i].Play();
        }
    }

    public void Hit()
    {
        if (canBeHitted)
        {
            overLay.GotHit();
            canBeHitted = false;
            UnityAction unityAction = null;
            unityAction += ToggleHitBool;
            StartCoroutine(Extensions.Wait(1, unityAction));
        }

            MyRb.velocity = Vector3.zero;
    }

    public void ToggleHitBool()
    {
        canBeHitted = true;
    }

    public void OnCollisionEnter(Collision coll)
    {
        CannonBall cannonBall = coll.gameObject.GetComponent<CannonBall>();
        if (cannonBall)
        {
            cannonBall.CannonEffect();
            Hit();
        }
    }

    public void OnBecameInvisible() => Destroy(this.gameObject);
}
