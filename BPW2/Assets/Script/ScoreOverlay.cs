﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreOverlay : MonoBehaviour
{
    [SerializeField]
    private Image[] hearths;
    private int score;
    [SerializeField]
    private Text text;

    public void Start()
    {
        text.text = "Score: " + score;
    }

    public void GotHit()
    {
        for (int i = 2; i > -1; i--)
            if (hearths[i].gameObject.activeInHierarchy)
            {
                hearths[i].gameObject.SetActive(false);
                break;
            }

        //Checking if ship is dead
        if (hearths[0].gameObject.activeInHierarchy == false)
        {
            int bestOfSea = PlayerPrefs.GetInt("Score");
            Debug.Log(bestOfSea < score);
            if(bestOfSea < score)
                PlayerPrefs.SetInt("Score", score);

            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        }

    }

    public void AddScore(int scoreToAdd)
    {
        score += scoreToAdd;
        text.text = "Score:" + score;
    }


}
