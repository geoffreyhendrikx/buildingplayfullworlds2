﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{
    [SerializeField]
    private bool loadNextScene;
    private void Awake()
    {
        DontDestroyOnLoad(this);

        if (loadNextScene)
            UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }
}
