﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PirateTimer : MonoBehaviour
{
    [SerializeField]
    [Range(0,1)]
    private float pirateStopWatch;
    public float PirateStopWatch
    {
        get
        {
            return pirateStopWatch;
        }
        set
        {
            pirateStopWatch = value;
        }
    }

    private bool timerToggle;
    public bool TimerToggle
    {
        get
        {
            return timerToggle;
        }
        set
        {
            timerToggle = value;
        }
    }


    public void ResetTimer()
    {
        timerToggle = false;
        pirateStopWatch = 1;
    }
    public void BeginTimer()
    {
        timerToggle = true;
        pirateStopWatch = 1;
    }
    // Update is called once per frame
    private void Update()
    {
        if (timerToggle)
            pirateStopWatch -= Time.deltaTime;
    }
}
