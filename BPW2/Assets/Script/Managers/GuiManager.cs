﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiManager : Manager
{
    /// <summary>
    /// Used for Best of Sea Highscore
    /// </summary>
    private Text bestOfSea;

    public GuiManager(Text bestOfSea)
    {
        this.bestOfSea = bestOfSea;
    }

    // Start is called before the first frame update
    public override void Start()
    {
        int score = PlayerPrefs.GetInt("Score");
        bestOfSea.text = string.Format("Best of Sea: " + score + " Points");
    }


    // Update is called once per frame
    public override void Update() { }
    
}
