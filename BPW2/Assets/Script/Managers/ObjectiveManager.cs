﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveManager : Manager
{
    private int index;
    public int Index
    {
        get
        {
            return index;
        }
        set
        {
            index = value;
        }
    }

    /// <summary>
    /// Objectives;
    /// </summary>
    private Objective[] objectives;
    public Objective[] Objectives
    {
        get
        {
            return objectives;
        }
        set
        {
            objectives = value;
        }
    }

    private Text information;
    public Text Information
    {
        get
        {
            return information;
        }
        set
        {
            information = value;
        }
    }



    public ObjectiveManager(Text information, Objective[] objectives)
    {
        this.information = information;
        this.objectives = objectives;
    }

    /// <summary>
    /// Getting the next objective.
    /// </summary>
    public void GetNextObjective()
    {
        information.text = objectives[index].MissionInformation;
    }
}
