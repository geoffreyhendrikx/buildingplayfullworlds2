﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteRunnerInputManager : MonoBehaviour
{
    [SerializeField]
    private BoatController boatController;
    [SerializeField]
    private PirateShip pirateShip;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.LeftArrow))
            boatController.MovingLane(-1);
        if( Input.GetKeyUp(KeyCode.RightArrow))
            boatController.MovingLane(1);

        if (pirateShip.canCheck)
            if (Input.GetKeyUp(pirateShip.KeyBind.KeyCode))
            {
                boatController.Attack();
                pirateShip.CanShoot = false;
            }

        if (Input.GetKey(KeyCode.Escape))
            Application.Quit();
    }
}
